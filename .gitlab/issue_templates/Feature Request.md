# Source
(Write here who is requesting the feature)
# What
(What the feature is about)
# Why
(What is the reason for needing this feature)
# Solution
(Possible ways to solve this issue)
# How to measure success
(How to find out that we are succesful)

/label ~"Feature Request"
/cc @timecatler
