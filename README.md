# StihiParser
Little Java project to parse poems from www.stihi.ru
## Sources
Direct link to sources folder [here](https://gitlab.com/eltech-scrum-masters/StihiParser/tree/master/src/main/java/ga/timecatler)
## Building
### Prerequisites
* Apache Maven
* Java jdk 14+

If you want to build an `.exe`
* Excelsior Jet
### Jar
Project  is configured to build a *fat jar* containing all dependencies using

`mvn clean package`

Produced artifact is at `target/StihiParser-%version%-jar-with-dependencies.jar`

You can use it via command line with

`java -jar StihiParser-%version%-jar-with-dependencies.jar --help`

### Exe
Change `jetHome` configuration variable in `pom.xml` to your Excelsior Jet location.

`mvn jet:build`

First run may take some time.

Produced executable with all dependencies is at `jet/StihiParser-%version%.zip`
