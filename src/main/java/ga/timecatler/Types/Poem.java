package ga.timecatler.Types;

import java.io.File;

/**
 * Represents poem
 */
public class Poem {
    private String Header = null;
    private String Text = null;
    private String URL = null;
    private File Image = null;

    public Poem() {}

    public Poem(String header, String text) {
        Header = header;
        Text = text;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public File getImage() {
        return Image;
    }

    public void setImage(File image) {
        Image = image;
    }

    @Override
    public String toString() {
        return "Poem{" +
                "Header='" + Header + '\'' +
                ", Text='" + Text + '\'' +
                ", URL='" + URL + '\'' +
                ", Image='" + Image.toString() + '\'' +
                '}';
    }
}
