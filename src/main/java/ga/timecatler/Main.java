package ga.timecatler;

import ga.timecatler.Core.AuthorScrapper;
import ga.timecatler.Core.IScrapper;
import ga.timecatler.Storage.DocxFile;
import ga.timecatler.Storage.IFile;
import ga.timecatler.Types.Poem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@CommandLine.Command(description = "Scraps poems for author page from stihi.ru.",
        name = "StihiScrapper", mixinStandardHelpOptions = true, version = "StihiScrapper 0.1")
public class Main implements Runnable {
    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Option(names = {"-v", "--verbose"}, description = "Verbose mode.")
    public void setVerboseMode(boolean mode) {
        Config.Constants.VERBOSE_MODE = mode;
    }

    @Option(names = {"--debug"}, description = "Use debug mode (load only first 50 poems)")
    public void setDebugMode(boolean mode) {
        Config.Constants.DEBUG_MODE = mode;
    }

    @Option(names = {"-a", "--author-format"}, description = "Generate output file with some additional links "
            + "useful for authors only")
    public void setAuthorRepresentation(boolean mode) {
        Config.Constants.AUTHOR_REPRESENTATION = mode;
    }

    @Option(names = {"-o", "--output"}, arity = "1", description = "Output file path")
    private Path output;

    private static boolean isValidAuthorLink(String link) throws IOException {
        Document page = Jsoup.connect(link).get();
        Elements selected = page.select("h1");
        return !selected.first()
                .childNodes().get(0)
                .toString().equals("Автор не найден");
    }

    private String author = "";

    /**
     * Validates 'author' parameter and sets it or throws error message
     * indicating problems with parameter
     *
     * @param input Ordered parameter extracted from arguments by picocli
     */
    @Parameters(index = "0", arity = "1", description = "Author page link or account name")
    public void setAuthor(String input) {
        String urlRegex = "(https?://)?(www.)?stihi.ru/avtor/\\w+";
        String authorRegex = "\\w+";
        // When input seems legit
        if (input.matches(authorRegex) || input.matches(urlRegex)) {
            String authorLink;

            String stihiPrefix = "https://stihi.ru/avtor/";
            // If only author add prefix otherwise use directly
            if (input.matches(authorRegex))
                authorLink = stihiPrefix + input;
            else
                authorLink = input;

            // If you can't connect to internet Jsoup throws an exception
            // Then we catch it and throw exception of our own (handled by picocli)
            try {
                // If author page doesn't exist, throw exception (handled by picocli)
                if (isValidAuthorLink(authorLink))
                    author = authorLink;
                else
                    throw new CommandLine.ParameterException(spec.commandLine(),
                            String.format("Supplied <author> argument '%s' doesn't specify a valid author page at '%s'.",
                                    input, authorLink));
            } catch (IOException e) {
                throw new CommandLine.ParameterException(spec.commandLine(),
                        "Unable to connect to internet");
            }
        } else {
            throw new CommandLine.ParameterException(spec.commandLine(),
                    String.format("Supplied <author> argument '%s' is not a valid input.", input));
        }
    }

    public void run() {
        IScrapper<List<Poem>> scrapper = new AuthorScrapper();
        scrapper.loadPage(author);
        IFile file = new DocxFile();
        file.fromResult(scrapper.presentResult());
        file.saveToPath(output.toAbsolutePath().toString());
    }

    public static void main(String[] args) {
        CommandLine.run(new Main(), args);
    }
}
