package ga.timecatler;

public class Config {
    public static class Constants {
        public static boolean DEBUG_MODE;
        public static boolean AUTHOR_REPRESENTATION;
        public static boolean VERBOSE_MODE;
    }
}
