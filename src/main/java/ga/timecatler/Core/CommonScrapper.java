package ga.timecatler.Core;

import ga.timecatler.Config;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Abstract class, containing default implementation of a {@link IScrapper#loadPage} method
 *
 * @param <T> Result type
 */
public abstract class CommonScrapper<T> implements IScrapper<T> {
    Document webpage;

    /**
     * Loads page using {@link Jsoup#connect}
     *
     * @param url URL
     */
    @Override
    public void loadPage(String url) {
        try {
            webpage = Jsoup.connect(url).get();
            // TODO: Replace with logging
            if (Config.Constants.VERBOSE_MODE)
                System.out.println("Loading page from: " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    abstract public T presentResult();
}
