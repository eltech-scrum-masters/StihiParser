package ga.timecatler.Core;

/**
 * Generic scrapper interface
 *
 * @param <T> Result parameter for a Scrapper
 */
public interface IScrapper<T> {
    /**
     * Load webpage by url for scrapping
     *
     * @param url URL
     */
    void loadPage(String url);

    /**
     * Present result based on loaded webpage
     *
     * @return Scrapping result
     */
    T presentResult();
}
