package ga.timecatler.Core;

import ga.timecatler.Types.Poem;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import ga.timecatler.Config;

/**
 * Loads and scraps all poems for author from a direct link to author page
 */
public class AuthorScrapper extends CommonScrapper<List<Poem>> implements IScrapper<List<Poem>> {
    /**
     * Finds and collects all poem urls on author page
     *
     * @return List of poem urls
     * @throws IllegalStateException When {@link #loadPage} is not called prior to this method
     */
    private List<String> scrapURLs() throws IllegalStateException {
        if (webpage == null)
            throw new IllegalStateException("loadPage method of IScrapper should be called before presentResult");
        List<String> res = new ArrayList<>();
        String baseUrl = webpage.baseUri();
        /* Server generates pages with 50 poems on one page
         *  Every page gets an address as .../%author%/&s=%number%
         *  Where %number% = 0 mod 50
         *  Server continues generating pages when poems are over
         */
        Integer POEMS_PER_PAGE = 50;
        for (Integer i = 0; ; i += POEMS_PER_PAGE) {
            loadPage(baseUrl + "&s=" + i.toString());

            /* Links to poems have a poemlink class
             * That makes life a lot easier
             */
            Elements PoemURLs = webpage.select("a[href].poemlink");
            if (PoemURLs.isEmpty())
                return res;
            PoemURLs.forEach(url -> res.add(url.attr("abs:href")));
            
            if (Config.Constants.DEBUG_MODE) return res;
        }

    }

    /**
     * Extracts poem urls using {@link #scrapURLs} after page is loaded by {@link #loadPage}
     * Then scraps poems for each url using {@link PoemScrapper}
     *
     * @return List of poems found on page
     */
    @Override
    public List<Poem> presentResult() {
        List<String> urls = scrapURLs();
        IScrapper<Poem> poemScrapper = new PoemScrapper();
        List<Poem> poems = new ArrayList<>();
        urls.forEach(url -> {
            poemScrapper.loadPage(url);
            poems.add(poemScrapper.presentResult());
        });
        return poems;
    }
}
