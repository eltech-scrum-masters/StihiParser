package ga.timecatler.Storage;

import ga.timecatler.Types.Poem;

import java.util.List;

/**
 * Interface for basic file abstraction
 */
public interface IFile {
    /**
     * Loads scrapping result into a file class
     *
     * @param result List of scrapped poems
     */
    void fromResult(List<Poem> result);

    /**
     * Saves file to path
     *
     * @param path Where to save file (with filename and extension)
     */
    void saveToPath(String path);

}
